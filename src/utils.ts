export const randomInt = (min: number, max: number): number => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
};

export const randomChoice = (arr: unknown[], n: number): unknown[] =>
  randomPermutation(0, arr.length, n).map((i) => arr[i]);

// O(n) might be slow.
export const randomPermutation = (
  low: number,
  high: number,
  k: number
): number[] => {
  const len = high - low;
  const arr = Array(len);

  for (let i = 0; i < len; i++) {
    arr[i] = low + i;
  }

  for (let i = 0; i < k; i++) {
    const j = randomInt(i, len);
    const tmp = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
  }

  return arr.slice(0, k);
};

export const replaceAt = (
  original: string,
  index: number,
  replacement: string
): string => {
  if(index >= original.length) {
    index = original.length-1;
  }
  const res = original.substr(0, index) +
        replacement +
        original.substr(index + replacement.length);
  return res;
}

export function fisherGatesFixed(word: string, percentage: number): string {
  let numbers: number[] = new Array(word.length);
  var divition = Math.ceil(word.length * percentage / 100);

  for (let i = 0; i < word.length; i++) {
    numbers[i] = i;
  }

  for (let i = 0; i < word.length; i++) {
    var j = randomInt(i, word.length);
    var temp = numbers[i];
    numbers[i] = numbers[j];
    numbers[j] = temp;
  }

  for (let i = 0; i < divition; i++) {
    word = replaceAt(word, numbers[i], word.charAt(numbers[i]).toUpperCase());
  }

  return word;
}

export function separatePassword(password: string, separator: string): string {
  var i: number = 45;

  if(password.length < 45) {
    return password
  }

  while((password.charAt(i) != separator) && (i > 0)) {
    i--;
  }
  
  password = password.substring(0, i+1) + 
             '<br/>' +
             separatePassword(password.substring(i+1, password.length), separator);

  return password;
}
