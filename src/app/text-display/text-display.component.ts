import { Component, Input, OnInit } from '@angular/core';
import { parse } from 'node:path';
import { generatePassword } from 'src/generatePassword';
import { separatePassword } from 'src/utils';

@Component({
  selector: 'app-text-display',
  templateUrl: './text-display.component.html',
  styleUrls: ['./text-display.component.css'],
})

export class TextDisplayComponent {
  @Input() password: string | undefined ;

  ngOnInit() {
    this.password = generatePassword(4, 7, 2, '-');
  }

  change(W: number, N: number, U: number, S: string): void {
    this.password = generatePassword(W, N, U, S);
    if(this.password.length >= 45) {
      var S: string;
      S = (document.getElementById("separator-input") as HTMLSelectElement)?.value;
      this.password = separatePassword(this.password, S);
    }
  };

  generateNewPassword(): void {
    var W: string;
    var N: string;
    var U: string;
    var S: string;
    W = (document.getElementById("word-input") as HTMLInputElement)?.value;
    N = (document.getElementById("number-input") as HTMLInputElement)?.value;
    U = (document.getElementById("upperCase-input") as HTMLSelectElement)?.value;
    S = (document.getElementById("separator-input") as HTMLSelectElement)?.value;

    this.password = generatePassword(parseInt(W), parseInt(N), parseInt(U), S);
    this.password = separatePassword(this.password, S);
  };

  copyToClipboard(): void {
    var pt = (document.getElementById("password-text") as HTMLElement)?.innerText;
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = pt;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  };
}
