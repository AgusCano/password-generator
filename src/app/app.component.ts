import { templateJitUrl } from '@angular/compiler';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Component } from '@angular/core';
import { platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { logWarnings } from 'protractor/built/driverProviders';
import { TextDisplayComponent } from 'src/app/text-display/text-display.component';
import { generatePassword } from 'src/generatePassword';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent {
  title = 'password-generator';
  passwordDisplay: TextDisplayComponent = new TextDisplayComponent();
  wordsN: number = 4;
  numbersN: number = 7;
  uppcasePct: number = 40;
  separator: string = '-';
  
  ngOnInit(): void {
    this.passwordDisplay.password = generatePassword(4, 7, 40, '-');
  };
  
  retrieveWordsNumber(event: any): void {
    var wordsN = parseInt((event.target as HTMLInputElement).value);
    this.wordsN = wordsN;
    this.passwordDisplay.change(wordsN, this.numbersN, this.uppcasePct, this.separator);
  };

  retrieveNumbersNumber(event: any): void {
    var numbersN = parseInt((event.target as HTMLInputElement).value);
    this.numbersN = numbersN;
    this.passwordDisplay.change(this.wordsN, numbersN, this.uppcasePct, this.separator);
  };

  retrieveUppcasePct(event: any): void {
    var uppcasePct = parseInt((event.target as HTMLSelectElement).value);
    this.uppcasePct = uppcasePct;
    this.passwordDisplay.change(this.wordsN, this.numbersN, uppcasePct, this.separator);
  };

  retrieveSeparator(event: any): void {
    var separator = (event.target as HTMLSelectElement).value;
    this.separator = separator;
    this.passwordDisplay.change(this.wordsN, this.numbersN, this.uppcasePct, separator);
  };

  fixRange(event: any, id: string): void{
    var value = (event.target as HTMLInputElement).value;
    (document.getElementById(id) as HTMLInputElement).value = value;
  };
}
