import { englishWords } from './assets/data/diceware-en';
import { randomInt, replaceAt, fisherGatesFixed} from './utils';

export const generatePassword = (
  numWords: number,
  numNumbers: number,
  uppcasePct: number,
  separator: string
): string => {
  const words = Array(numWords);
  for (let i = 0; i < numWords; i++) {
    words[i] = englishWords[randomInt(0, englishWords.length)];
  }

  const numbers = Array(numNumbers);
  for (let i = 0; i < numNumbers; i++) {
    numbers[i] = randomInt(0, 10);
  }

  for (let i = 0; i < numWords; i++) {
    words[i] = fisherGatesFixed(words[i], uppcasePct);
  }

  const all = Array(0);
  if (numbers.length > 0 && Math.random() < 0.5) {
    all.push(`${numbers.pop()}`);
  }

  while (words.length != 0) {
    all.push(words.pop());
    if (numbers.length > 0 && Math.random() < 0.5) {
      all.push(`${numbers.pop()}`);
      all.push(separator)
    } else {
      all.push(separator);
    }
  }

  while (numbers.length != 0) {
    all.push(numbers.pop());
  }

  return all.join('');
};
